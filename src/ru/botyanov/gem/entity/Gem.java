package ru.botyanov.gem.entity;

import ru.botyanov.gem.enums.Color;

public class Gem {
    private String id;
    private String name;
    private String origin;
    private double value;
    private VisualParameters visualParameters;

    public Gem() {
    }

    public Gem(String name, String origin, double value, VisualParameters visualParameters, String id) {
        this.name = name;
        this.origin = origin;
        this.value = value;
        this.visualParameters = visualParameters;
        this.id = id;
    }

    public static class VisualParameters {
        private Color color;
        private int transparency;
        private int cuts;

        public VisualParameters() {
        }

        public void setColor(Color color) {
            this.color = color;
        }

        public void setTransparency(int transparency) {
            this.transparency = transparency;
        }

        public void setCuts(int cuts) {
            this.cuts = cuts;
        }

        public Color getColor() {
            return color;
        }

        public int getTransparency() {
            return transparency;
        }

        public int getCuts() {
            return cuts;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Visual{");
            sb.append("color=").append(color);
            sb.append(", transparency=").append(transparency);
            sb.append(", cuts=").append(cuts);
            sb.append('}');
            return sb.toString();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", origin='").append(origin).append('\'');
        sb.append(", value=").append(value);
        sb.append(", visualParameters=").append(visualParameters);
        sb.append('}');
        return sb.toString();
    }
}