package ru.botyanov.gem.entity;

public class SemiPreciousGem extends Gem {
    private boolean polished;

    public SemiPreciousGem() {
    }

    public SemiPreciousGem(String name, String origin, double value,
                           VisualParameters visualParameters, boolean polished, String id) {
        super(name, origin, value, visualParameters, id);
        this.polished = polished;
    }

    public boolean isPolished() {
        return polished;
    }

    public void setPolished(boolean polished) {
        this.polished = polished;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SemiPreciousGem{");
        sb.append(super.toString());
        sb.append(", polished=").append(polished);
        sb.append('}');
        return sb.toString();
    }
}