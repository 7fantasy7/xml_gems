package ru.botyanov.gem.enums;

public enum CutStyle {
    BRILLIANT, CABOCHON, EMERALD, HEART, MOGUL, OBUS, OVAL, ROUND, PRINCESS, TRILLIANT
}