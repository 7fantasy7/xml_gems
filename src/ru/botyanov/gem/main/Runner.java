package ru.botyanov.gem.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import ru.botyanov.gem.parser.GemDOMBuilder;
import ru.botyanov.gem.parser.GemSAXBuilder;
import ru.botyanov.gem.parser.GemSTAXBuilder;
import ru.botyanov.gem.validator.GemValidator;

public class Runner {
    private static final String LOG_PATH = "config/log4j.xml";
    private static final Logger LOG = Logger.getLogger(Runner.class);
    public static final String FILE = "files/gem.xml";

    static {
        new DOMConfigurator().doConfigure(LOG_PATH, LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        GemValidator.validate();
        GemDOMBuilder gemDOMBuilder = new GemDOMBuilder();
        gemDOMBuilder.buildSetStudents(FILE);
        gemDOMBuilder.getGems().forEach(LOG::info);
        GemSAXBuilder.parse();
        GemSAXBuilder.getGems().forEach(LOG::info);
        GemSTAXBuilder.parse();
        GemSTAXBuilder.getGems().forEach(LOG::info);
    }
}