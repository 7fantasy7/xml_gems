package ru.botyanov.gem.parser;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.botyanov.gem.entity.Gem;
import ru.botyanov.gem.entity.PreciousGem;
import ru.botyanov.gem.entity.SemiPreciousGem;
import ru.botyanov.gem.enums.Color;
import ru.botyanov.gem.enums.CutStyle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class GemDOMBuilder {
    private static final Logger LOG = Logger.getLogger(GemDOMBuilder.class);
    private Set<Gem> gems = new HashSet<>();
    private DocumentBuilder docBuilder;

    public GemDOMBuilder() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error("Parser configuration excepion: " + e);
        }
    }

    public Set<Gem> getGems() {
        return gems;
    }

    public void buildSetStudents(String fileName) {
        Document doc = null;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();

            buildElements(root,"semiprecious-gem");
            buildElements(root, "precious-gem");
        } catch (IOException e) {
            LOG.error("File error or I/O error: " + e);
        } catch (SAXException e) {
            LOG.error("Parsing failure: " + e);
        }
    }

    private void buildElements(Element root,String gemType) {
        NodeList gemList = root.getElementsByTagName(gemType);
        for (int i = 0; i < gemList.getLength(); i++) {
            Element gemElement = (Element) gemList.item(i);
            Gem gem = buildGem(gemElement, false);
            gems.add(gem);
        }
    }

    private Gem buildGem(Element gemElement, boolean preciousness) {
        Gem gem;

        String name = getElementTextContent(gemElement, "name");
        String origin = getElementTextContent(gemElement, "origin");
        Double value = Double.parseDouble(getElementTextContent(gemElement, "value"));
        String id = gemElement.getAttribute("id");

        String vParams = getElementTextContent(gemElement, "visual-parameters");
        String[] mas = vParams.trim().replaceAll("\\s+", " ").split(" ");

        Gem.VisualParameters visualParameters = new Gem.VisualParameters();
        visualParameters.setColor(Color.valueOf(mas[0].toUpperCase()));
        visualParameters.setTransparency(Integer.valueOf(mas[1]));
        visualParameters.setCuts(Integer.valueOf(mas[2]));

        if (preciousness) {
            PreciousGem pGem = new PreciousGem();
            makeGem(name, origin, value, id, visualParameters, pGem);
            pGem.setCutStyle(CutStyle.valueOf(getElementTextContent(gemElement, "cut-style").toUpperCase()));
            gem = pGem;
        } else {
            SemiPreciousGem sGem = new SemiPreciousGem();
            makeGem(name, origin, value, id, visualParameters, sGem);
            sGem.setPolished(Boolean.valueOf(getElementTextContent(gemElement, "polished")));
            gem = sGem;
        }
        return gem;
    }

    private static void makeGem(String name, String origin, Double value, String id, Gem.VisualParameters visualParameters, Gem gem) {
        gem.setName(name);
        gem.setOrigin(origin);
        gem.setId(id);
        gem.setValue(value);
        gem.setVisualParameters(visualParameters);
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent();
    }
}