package ru.botyanov.gem.parser;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.botyanov.gem.entity.Gem;
import ru.botyanov.gem.entity.PreciousGem;
import ru.botyanov.gem.entity.SemiPreciousGem;
import ru.botyanov.gem.enums.Color;
import ru.botyanov.gem.enums.CutStyle;

import java.util.HashSet;

import static ru.botyanov.gem.parser.PropertyReader.takeTags;

public class GemHandler extends DefaultHandler {
    private static final Logger LOG = Logger.getLogger(GemHandler.class);
    private HashSet<Gem> gems = new HashSet<>();
    private Gem current;
    private String tag;
    private boolean flag;

    public GemHandler() {
    }

    @Override
    public void startDocument() throws SAXException {
        LOG.info("SAX parsing started...");
    }

    @Override
    public void endDocument() throws SAXException {
        LOG.info("SAX parsing finished...");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        tag = qName;
        if (takeTags("gem.tags").stream().anyMatch(s -> s.equals(tag))) {
            if (takeTags("gem.tags").get(0).equals(tag)) {
                current = new PreciousGem();
            } else if (takeTags("gem.tags").get(1).equals(tag)) {
                current = new SemiPreciousGem();
            }
            current.setId(attributes.getValue(0));
            flag = true;
        }
        if (takeTags("inner.tags").get(0).equals(tag)) {
            current.setVisualParameters(new Gem.VisualParameters());
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (flag) {
            gems.add(current);
            flag = false;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String param = new String(ch, start, length).trim();
        if (!param.isEmpty()) {
            if (takeTags("param.tags").get(0).equals(tag)) {
                current.setName(param);
            } else if (takeTags("param.tags").get(1).equals(tag)) {
                current.setOrigin(param);
            } else if (takeTags("param.tags").get(2).equals(tag)) {
                current.setValue(Double.valueOf(param));
            } else if (takeTags("param.tags").get(3).equals(tag)) {
                current.getVisualParameters().setColor(Color.valueOf(param.toUpperCase()));
            } else if (takeTags("param.tags").get(4).equals(tag)) {
                current.getVisualParameters().setTransparency(Integer.valueOf(param));
            } else if (takeTags("param.tags").get(5).equals(tag)) {
                current.getVisualParameters().setCuts(Integer.valueOf(param));
            } else if (takeTags("param.tags").get(6).equals(tag)) {
                ((PreciousGem) current).setCutStyle(CutStyle.valueOf(param.toUpperCase()));
            } else if (takeTags("param.tags").get(7).equals(tag)) {
                ((SemiPreciousGem) current).setPolished(Boolean.valueOf(param));
            }
        }
    }

    public HashSet<Gem> getGems() {
        return gems;
    }
}