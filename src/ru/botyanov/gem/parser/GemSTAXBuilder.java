package ru.botyanov.gem.parser;

import org.apache.log4j.Logger;
import ru.botyanov.gem.entity.Gem;
import ru.botyanov.gem.entity.PreciousGem;
import ru.botyanov.gem.entity.SemiPreciousGem;
import ru.botyanov.gem.enums.Color;
import ru.botyanov.gem.enums.CutStyle;
import ru.botyanov.gem.main.Runner;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;

import static ru.botyanov.gem.parser.PropertyReader.takeTags;

public class GemSTAXBuilder {
    private static final Logger LOG = Logger.getLogger(GemSTAXBuilder.class);
    private static HashSet<Gem> gems = new HashSet<>();
    private static Gem current;
    private static String tag;

    public static void parse() {
        LOG.info("STAX parsing started...");
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try (FileInputStream input = new FileInputStream(new File(Runner.FILE))) {
            XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    tag = reader.getLocalName();
                    if (takeTags("gem.tags").stream().anyMatch(s -> s.equals(tag))) {
                        current = buildGem(reader);
                        gems.add(current);
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            LOG.error(e);
        }
        LOG.info("STAX parsing finished...");
    }

    private static Gem buildGem(XMLStreamReader reader) throws XMLStreamException {
        if (takeTags("gem.tags").get(0).equals(tag)) {
            current = new PreciousGem();
        } else if (takeTags("gem.tags").get(1).equals(tag)) {
            current = new SemiPreciousGem();
        }
        current.setId(reader.getAttributeValue(null, "id"));
        current.setVisualParameters(new Gem.VisualParameters());

        try {
            while (reader.hasNext()) {
                int cursor = reader.next();
                if (cursor == XMLStreamConstants.START_ELEMENT) {
                    tag = reader.getLocalName();
                    if (takeTags("param.tags").get(0).equals(tag)) {
                        current.setName(getXMLText(reader));
                    } else if (takeTags("param.tags").get(1).equals(tag)) {
                        current.setOrigin(getXMLText(reader));
                    } else if (takeTags("param.tags").get(2).equals(tag)) {
                        current.setValue(Double.valueOf(getXMLText(reader)));
                    } else if (takeTags("param.tags").get(3).equals(tag)) {
                        current.getVisualParameters().setColor(Color.valueOf(getXMLText(reader).toUpperCase()));
                    } else if (takeTags("param.tags").get(4).equals(tag)) {
                        current.getVisualParameters().setTransparency(Integer.valueOf(getXMLText(reader)));
                    } else if (takeTags("param.tags").get(5).equals(tag)) {
                        current.getVisualParameters().setCuts(Integer.valueOf(getXMLText(reader)));
                    } else if (takeTags("param.tags").get(6).equals(tag)) {
                        ((PreciousGem) current).setCutStyle(CutStyle.valueOf(getXMLText(reader).toUpperCase()));
                    } else if (takeTags("param.tags").get(7).equals(tag)) {
                        ((SemiPreciousGem) current).setPolished(Boolean.valueOf(getXMLText(reader)));
                    }
                } else if (cursor == XMLStreamConstants.END_ELEMENT) {
                    tag = reader.getLocalName();
                    if (takeTags("gem.tags").stream().anyMatch(s -> s.equals(tag))) {
                        return current;
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOG.error(e);
        }
        throw new XMLStreamException("Unknown element in tag Device");
    }

    private static String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

    public static HashSet<Gem> getGems() {
        return gems;
    }
}
