package ru.botyanov.gem.parser;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class PropertyReader {
    private static final Logger LOG = Logger.getLogger(PropertyReader.class);
    private static final String PROP_PATH = "properties/gems.properties";

    public static ArrayList<String> takeTags(String tagsType) {
        Properties properties = new Properties();
        ArrayList<String> tags = new ArrayList<>();
        try {
            properties.load(new FileInputStream(PROP_PATH));
            tags.addAll(Arrays.asList(properties.getProperty(tagsType).split(";")));
        } catch (IOException e) {
            LOG.error(e);
        }
        return tags;
    }
}