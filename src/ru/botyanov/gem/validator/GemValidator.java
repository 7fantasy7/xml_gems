package ru.botyanov.gem.validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import ru.botyanov.gem.main.Runner;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class GemValidator {
    private static final Logger LOG = Logger.getLogger(GemValidator.class);
    private static final String SCHEMA = "files/gem.xsd";

    public static void validate() {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File schemaLocation = new File(SCHEMA);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(Runner.FILE);
            validator.validate(source);
            LOG.info("File " + Runner.FILE + " is valid.");
        } catch (SAXException e) {
            LOG.error("File " + Runner.FILE + " isn't valid: " + e.getMessage());
        } catch (IOException e) {
            LOG.error("Input error " + e.getMessage() + " in file " + Runner.FILE);
        }
    }
}